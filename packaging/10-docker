#!/bin/bash

# Check firewall flag
#--------------------

RUN_HOOK=yes

if [ -e /etc/clearos/docker.conf ]; then
    CHECK=$(grep -i '^enable_firewall[[:space:]]*=[[:space:]]*no' /etc/clearos/docker.conf 2>/dev/null)
    if [ -n "$CHECK" ]; then
        RUN_HOOK=no
    fi
fi

IFACE_EXISTS=$(ifconfig docker0 2>/dev/null)

if [ -z "$IFACE_EXISTS" ]; then
    brctl addbr docker0
fi

# Firewall hook
#--------------

if [ "$RUN_HOOK" == 'yes' ]; then

    # Check if $IPTABLES is set. This allows the program to run outside control of the firewall.
    # i.e on Docker start. If running under firewall control, $IPTABLES will have been set and the
    # base rules and chains would have been cleared and need to be re-added
    #---------------------------------------------------------------------------------------------
    if [ -z "$IPTABLES" ] ; then
        IPTABLES='/usr/sbin/iptables -w'
        
        # Clear any old MASQUERADE rules in case the Docker IP has changed
        #-----------------------------------------------------------------
        RULE_IDS=$($IPTABLES -nv --line-numbers -t nat -L POSTROUTING |\
            grep 'docker0' | grep MASQUERADE | awk '{ print $1 }' | sort -rn)
        if [ -n "$RULE_IDS" ]; then
            for rule_id in $RULE_IDS; do
                $IPTABLES -t nat -D POSTROUTING ${rule_id}
            done
        fi

    else

        # This will bail if the script runs as part of the firewall restart and is not ipv4
        if [ "$FW_PROTO" != 'ipv4' ]; then
            return 0
        fi

        # Add Docker chains
        #--------------

        $IPTABLES -t nat -N DOCKER 2>/dev/null
        $IPTABLES -t filter -N DOCKER 2>/dev/null
        $IPTABLES -t filter -N DOCKER-ISOLATION 2>/dev/null

        # NAT table
        #----------

        $IPTABLES -t nat -A PREROUTING -m addrtype --dst-type LOCAL -j DOCKER
        $IPTABLES -t nat -A OUTPUT ! -d 127.0.0.0/8 -m addrtype --dst-type LOCAL -j DOCKER

        # Filter table
        #-------------

        # If in standalone mode the generic related/established rule is missing
        $IPTABLES -t filter -C FORWARD -m state  --state  RELATED,ESTABLISHED -j ACCEPT 2>/dev/null
        if [ $? -ne 0 ]; then
             $IPTABLES -t filter -A FORWARD -m state  --state  RELATED,ESTABLISHED
        fi

        $IPTABLES -t filter -A FORWARD -j DOCKER-ISOLATION
        $IPTABLES -t filter -A FORWARD -o docker0 -j DOCKER
        $IPTABLES -t filter -A FORWARD -i docker0 -j ACCEPT

        $IPTABLES -t filter -A INPUT -i docker0 -j ACCEPT
        $IPTABLES -t filter -A OUTPUT -o docker0 -j ACCEPT

    fi

    # Add back in the MASQUERADE rules if the docker0 interface has an IP address
    #----------------------------------------------------------------------------
    DOCKER_NETWORK=$(ip addr show docker0 | grep ^[[:space:]]*inet[[:space:]] | awk '{ print $2 }')
    if [ -n "$DOCKER_NETWORK" ]; then
        $IPTABLES -t nat -A POSTROUTING -s $DOCKER_NETWORK ! -o docker0 -j MASQUERADE
    fi
fi
